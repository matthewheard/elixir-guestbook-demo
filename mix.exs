defmodule Guestbook.MixProject do
  use Mix.Project

  def project do
    [
      app: :guestbook,
      version: "0.1.0",
      elixir: "~> 1.7",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger],
      mod: {Guestbook.Application, []}
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:cowboy, "~> 2.1"},
      {:ecto, "~> 2.1"},
      {:jason, "~> 1.0"},
      {:maru, "~> 0.13"},
      {:mix_test_watch, "~> 0.8", only: :dev, runtime: false},
      {:postgrex, ">= 0.0.0"}
    ]
  end
end
