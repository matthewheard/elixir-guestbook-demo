defmodule Guestbook.Application do
  use Application

  def start(_type, _args) do
    children = [
      Guestbook.Repo
    ]

    opts = [strategy: :one_for_one, name: Guestbook.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
