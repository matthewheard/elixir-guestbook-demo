defmodule Guestbook.Router.Homepage do
  use Maru.Router

  get do
    json(conn, Guestbook.Repo.all_entries())
  end
end
