defmodule Guestbook.API do
  use Maru.Router

  plug(Plug.Parsers,
    pass: ["*/*"],
    json_decoder: Poison,
    parsers: [:urlencoded, :json, :multipart]
  )

  mount(Guestbook.Router.Homepage)

  params do
    requires(:body, type: String)
  end

  post :entries do
    repo = Guestbook.Repo
    params[:body] |> Guestbook.insert(repo)

    conn |> put_status(201) |> text("Created")
  end

  rescue_from :all do
    conn |> put_status(500) |> text("Server Error")
  end
end
