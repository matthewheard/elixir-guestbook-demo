defmodule Guestbook.Entry do
  use Ecto.Schema

  schema "entries" do
    field(:body)
  end
end
