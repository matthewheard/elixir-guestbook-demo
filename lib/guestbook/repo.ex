defmodule Guestbook.Repo do
  use Ecto.Repo, otp_app: :guestbook

  alias Guestbook.Entry

  def all_entries do
    Entry |> all |> Enum.map(fn e -> e.body end)
  end

  def insert_entry(body) do
    %Guestbook.Entry{body: body} |> insert
  end
end
