defmodule Guestbook do
  @moduledoc """
  Documentation for Guestbook.
  """

  def all_entries(repo) do
    repo.all_entries
  end

  def insert(repo, entry_body) do
    entry_body |> repo.insert_entry
  end
end
