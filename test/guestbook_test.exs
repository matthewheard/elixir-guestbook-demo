defmodule GuestbookTest do
  use ExUnit.Case
  doctest Guestbook

  defmodule EmptyRepo do
    def all_entries, do: []
  end

  test "given an empty repo returns an empty list" do
    repo = EmptyRepo
    all_entries = Guestbook.all_entries(repo)
    expected_entries = []

    assert all_entries == expected_entries
  end

  defmodule RepoWithOneEntry do
    def all_entries, do: ["Hello world"]
  end

  test "given a repo with a single entry returns that entry" do
    repo = RepoWithOneEntry
    all_entries = Guestbook.all_entries(repo)
    expected_entries = ["Hello world"]

    assert all_entries == expected_entries
  end

  defmodule InsertableRepo do
    use Agent

    def setup do
      Agent.start_link(fn -> [] end, name: :repo)
    end

    def all_entries do
      Agent.get(:repo, fn list -> list end)
    end

    def insert_entry(body) do
      Agent.update(:repo, fn list -> [body | list] end)
    end
  end

  test "inserting an entry inserts it into the repo" do
    repo = InsertableRepo
    repo.setup

    assert Guestbook.all_entries(repo) == []

    Guestbook.insert(repo, "Hello world")

    assert Guestbook.all_entries(repo) == ["Hello world"]
  end
end
