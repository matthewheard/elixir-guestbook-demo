use Mix.Config

config :guestbook, Guestbook.Repo,
  adapter: Ecto.Adapters.Postgres,
  database: "guestbook_repo",
  username: "postgres",
  password: "postgres",
  hostname: "localhost"

config :guestbook, ecto_repos: [Guestbook.Repo]

config :maru, Guestbook.API, http: [port: 8880]
